package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan juan Camilo Castillo 1763929-2711
 */
import java.util.List;
import java.util.LinkedList;

public class OrgTransito {

    private List<Licencia> licencias = new LinkedList<>();
    private List<Agente> agentes = new LinkedList<>();
    private List<MotivoMulta> MoMultas = new LinkedList<>();
    private List<Multas> multas = new LinkedList<>();
    private List<Vehiculo> vehiculos = new LinkedList<>();
    private List<Persona>personas = new LinkedList<>();
    /////////////////////////////////////////////////////////////////////////////
    //constructor

    public OrgTransito() {
    }
    //////////////////////////////////////////////////////////////////////////////
    // METODOS AGREGAR, ELIMINAR Y BUSCAR "Licencia"

    public void addLicencias(Licencia Nlicencia) throws Exception {
        for (Licencia NL : licencias) {
            if (Nlicencia.equals(NL)) {
                throw new Exception("La licencia ya esta registrada");

            }
        }
        this.licencias.add(Nlicencia);
        System.out.println("se registro la licencia ");
    }

    public void removeLicencias(Licencia licenciae) throws Exception {
        for (Licencia L : licencias) {
            if (L.equals(licenciae)) {
                this.licencias.remove(licenciae);
                System.out.println("la licencia se elimino con exicto");
            }
        }
        throw new Exception("la licencia no fue encontada");
    }

    Licencia buscarLC(Licencia LC) throws Exception {
        if (licencias.contains(LC)) {
            return licencias.get(licencias.indexOf(LC));
        }
        throw new Exception("No se encontroi la licencia ");
    }
    ///////////////////////////////////////////////////////////////////////////////
    //METODOS AGREGAR, ELIMINAR Y BUSCAR "Agentes"

    public void addAgente(Agente NAgente) throws Exception {
        for (Agente A : agentes) {
            if (A.getIdentificacion() == NAgente.getIdentificacion()) {
                throw new Exception("El agente ya esta registrado");
            }
        }
        this.agentes.add(NAgente);
        System.out.println("El agente fue registrado");
    }

    public void removeAgente(Agente AgenteDes) {

        this.agentes.remove(AgenteDes);

    }

    public Agente BuscarAgente(short placa) throws Exception {
        int i = 0;//ITERADOR
        if (agentes == null) {
            throw new Exception("La lista estra vacia");
        } else {
            while (i <= agentes.size()) {
                if (agentes.get(i).getNumeroPlaca() == placa) {
                    return agentes.get(i);
                }
                i++;
            }
        }
        throw new Exception("No se encontro el agente");
    }

//    Agente BuscarAgente(Agente A) throws Exception {
//        if (agentes.contains(A)) {
//            return agentes.get(agentes.indexOf(A));
//        } else {
//            throw new Exception("el Agente no fue encontrado /n");
//        }
//    }
    ///////////////////////////////////////////////////////////////////////////////
    //METODOS AGREGAR, ELIMINAR Y BUSCAR "MotivoMulta"
    public void addMMulta(MotivoMulta MMulta) throws Exception {
        for (MotivoMulta MM : MoMultas) {
            if (MM.getCodigo() == MMulta.getCodigo()) {
                throw new Exception("El motivo multa ya esta agregado");
            }
        }
        this.MoMultas.add(MMulta);
        System.out.println("El motivo multa se agrego con exito");
    }

    MotivoMulta BuscarMM(MotivoMulta M) throws Exception {
        if (MoMultas.contains(M)) {
            return MoMultas.get(MoMultas.indexOf(M));
        } else {
            throw new Exception("no se encontro el motivo multa /n");

        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    //METODOS AGREGAR, ELIMINAR Y BUSCAR  "Multas"
    public void addMulta(Multas NMulta) throws Exception {
        for (Multas M : multas) {
            if (M.equals(NMulta)) {
                throw new Exception("la multra ya esta registrada");
            }
        }
        this.multas.add(NMulta);
        System.out.println("La multa se registro con exito");
    }

    public void removeMultas(Multas EMulta) throws Exception {

        this.multas.remove(EMulta);

    }

    Multas BuscarM(Multas M) throws Exception {
        if (multas.contains(M)) {
            return multas.get(multas.indexOf(M));
        } else {
            throw new Exception("No se enconto la multa");
        }

    }
    ///////////////////////////////////////////////////////////////////////////////
    //METODOS AGREGAR, ELIMINAR Y BUSCAR "Vehiculos"

    public void addVehiculo(Vehiculo NVehivuco) throws Exception {
        for (Vehiculo V : vehiculos) {
            if (V.getPlaca() == NVehivuco.getPlaca()) {
                throw new Exception("El vehiculo ya esta registrado");
            }
        }
        this.vehiculos.add(NVehivuco);
        System.out.println("El vehiculo se registro con exito");
    }

    void EliminarV(Vehiculo V) throws Exception {
        if (vehiculos.contains(V)) {
            this.vehiculos.remove(V);
            System.out.println("El vrhiculo se elimino con exito");
        } else {
            throw new Exception("no se encontro el vehiculo ha eliminar");
        }
    }

        //public Vehiculo BuscarV(String placa) throws Exception {
        // for (Vehiculo nv : vehiculos) {
    //            if ((nv.getPlaca())== placa) {
    //                return nv;
    //
    //            }else if(vehiculos.size)
    //                
    //                
    //                throw new Exception("no se encontro el vehiculo /n");
    //            }
    //                
//            } 
//
//        
//        }
    public Vehiculo BuscarV(String placa) throws Exception {
        int i = 0; //iterator
        if (vehiculos == null) {
            throw new Exception("La lista esta vacia");
        } else {
            while (i <= vehiculos.size()) {
                if (vehiculos.get(i).getPlaca().equals(placa)) {
                    return vehiculos.get(i);
                }
                i++;
            }

        }
        throw new Exception("el vehiculo no se encontro");

    }
    
     public List<Licencia>BuscarLicencia(long identificacion){
       LinkedList<Licencia> Encontradas = new LinkedList<>();
       for(Licencia licencia : licencias){
           if(licencia.getPersona().getIdentificacion()== identificacion){
               Encontradas.add(licencia);
           }
       }
       return Encontradas;
    
}
     
     ///////////////////////////////////////////////////////////////////////////////
     //BUSCAR PERSONA//////////////////////////////////////////////////////////////
      public    Persona BuscarPerona( long identificacion) throws Exception {
        int i = 0;//ITERADOR
        if ( personas== null) {
            throw new Exception("La lista estra vacia");
        } else {
            while (i <= personas.size()) {
                if (personas.get(i).getIdentificacion()== identificacion) {
                    return personas.get(i);
                }
                i++;
            }
        }
        throw new Exception("No se encontro la persona");
    }
}