package modelo;

import MultaUI.PrincipalUI;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan Camilo Castillo 1763929-2711
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {

        OrgTransito org = new OrgTransito();
        
        //objetos
        try{
            
            Persona P1 = new Persona(1113685071, "juan", "casrtillo");
            
        org.addAgente(new Agente((short)1234, 1113685071L, "juan", "castillo"));
        org.addAgente(new Agente((short)1235, 1113685072L, "juan", "castillo"));
        org.addAgente(new Agente((short)1236, 1113685081L, "juan", "castillo"));
        org.addVehiculo(new Vehiculo("123456", (short)2015,"azul", "skoda"));
        org.addVehiculo(new Vehiculo("113456", (short)2015,"rojo", "mazda"));
        org.addVehiculo(new Vehiculo("103456", (short)2015,"morado", "skoda"));

        PrincipalUI principalUI = new PrincipalUI(org);
        principalUI.setVisible(true);
        // TODO code application logic here
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date fecha = sdf.parse("2016/01/01");
        
        org.addLicencias(new Licencia((new Persona(1113685011L, "juancho", "peña")), CategoriaEnum.A1, fecha));
        org.addLicencias(new Licencia((new Persona(1113685012L, "Diego", "norrea")), CategoriaEnum.A2, fecha));
     }catch(Exception exc){
         
     }
    
}

}
