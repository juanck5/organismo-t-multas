package modelo;

import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan Camilo Castillo 1763929-2711
 */
public class Persona {

    private long identificacion;
    private String Nombre;
    private String Apellido;
    //constructor por defecto

    public Persona() {
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //CONSTRUCTOR

    public Persona(long identificacion, String Nombre, String Apellido) throws Exception {
        if (identificacion <= (long) 999999999L || identificacion >= (long) 99999999999L) {
            throw new Exception("ingrese una identificacion valida(10 digitos)");
        }
        if (Nombre == null || "".equals(Nombre.trim())) {
            throw new Exception("El nombre no puede ser vacio o null");
        }
        if (Apellido == null || "".equals(Apellido.trim())) {
            throw new Exception("El Apellido no puede ser vacio o null");
        }

        this.identificacion = identificacion;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    //METODOS GET

    public long getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    //METOSO SET
    public void setIdentificacion(long identificacion) throws Exception {
        if (identificacion <= (long) 999999999L || identificacion >= (long) 99999999999L) {
            throw new Exception("ingrese una identificacion valida(10 digitos)");
        }

        this.identificacion = identificacion;
    }

    public void setNombre(String Nombre) throws Exception {
        if (Nombre == null || "".equals(Nombre.trim())) {
            throw new Exception("El nombre no puede ser vacio o null");
        }
        this.Nombre = Nombre;
    }

    public void setApellido(String Apellido) throws Exception {
        if (Apellido == null || "".equals(Apellido.trim())) {
            throw new Exception("El Apellido no puede ser vacio o null");
        }
        this.Apellido = Apellido;

    }

    //metodo que muestra em pantalla los datos de una persona
    //void Persona(){
    //  System.out.println("identificacion:"+ getIdentificacion());
    //System.out.println("Nombre:" + getNombre() );
    //System.out.println("Apellido:"+ getApellido());
    //}
    /////////////////////////////////////////////////////////////////////////////////////////////////////////7
    //EQUALS
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.identificacion != other.identificacion) {
            return false;
        }
        if (!Objects.equals(this.Nombre, other.Nombre)) {
            return false;
        }
        if (!Objects.equals(this.Apellido, other.Apellido)) {
            return false;
        }
        return true;
    }

}
