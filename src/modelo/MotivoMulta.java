package modelo;

import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan Camilo Castillo 1763929-2711
 */
public class MotivoMulta {

    private short Codigo;
    private String Descripcion;
    private int Valor;

    public MotivoMulta() {
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR
    public MotivoMulta(short Codigo, String Descripcion, int Valor) throws Exception {
        if (Codigo <= 99 || Codigo > 999) {
            throw new Exception("el codigo debe tener tres digitos");
        }
        if (Descripcion == null || "".equals(Descripcion.trim())) {
            throw new Exception("la descripcion no puede ser null o vacia");
        }
        if (Valor <= 0) {
            throw new Exception("El valor no puede ser 0");
        }

        this.Codigo = Codigo;
        this.Descripcion = Descripcion;
        this.Valor = Valor;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //METODOS GET
    public short getCodigo() {
        return Codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public int getValor() {
        return Valor;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    //METODOS SET
    public void setCodigo(short Codigo) throws Exception {
        if (Codigo <= 99 || Codigo > 999) {
            throw new Exception("el codigo debe tener tres digitos");
        }
        this.Codigo = Codigo;
    }

    public void setDescripcion(String Descripcion) throws Exception {
        if (Descripcion == null || "".equals(Descripcion.trim())) {
            throw new Exception("la descripcion no puede ser null o vacia");
        }
        this.Descripcion = Descripcion;
    }

    public void setValor(int Valor) throws Exception {
        if (Valor <= 0) {
            throw new Exception("El valor no puede ser 0");
        }
        this.Valor = Valor;
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    // EQUALS
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MotivoMulta other = (MotivoMulta) obj;
        if (this.Codigo != other.Codigo) {
            return false;
        }
        if (this.Valor != other.Valor) {
            return false;
        }
        if (!Objects.equals(this.Descripcion, other.Descripcion)) {
            return false;
        }
        return true;
    }

}
