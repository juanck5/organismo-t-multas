package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan Camilo Castillo 1763929-2711
 */
import java.util.Date;
import java.util.Objects;

public class Licencia {

    private Date Date;
    private Persona persona;
    private CategoriaEnum Categoria;

    //////////////////////////////////////////////////////////////////////////////////////////
    //CONSTRUCTOR
    public Licencia(Persona persona, CategoriaEnum Categoria, Date Date) {
        this.persona = persona;
        this.Date = Date;
        this.Categoria = Categoria;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // METODOS GET
    public Persona getPersona() {
        return persona;
    }

    public CategoriaEnum getCategoria() {
        return Categoria;
    }
    
    public Date getDate(){
        return Date;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //EQUALS
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Licencia other = (Licencia) obj;
        if (!Objects.equals(this.persona, other.persona)) {
            return false;
        }
        if (this.Categoria != other.Categoria) {
            return false;
        }
        return true;
    }

}
