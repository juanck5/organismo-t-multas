package modelo;

import java.time.LocalDate;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan Camilo Castillo 1763929-2711
 */
public class Vehiculo {

    private String Placa;
    private short Modelo;
    private String Color;
    private String Marca;

    public Vehiculo() {
    }

    int Añovalido = LocalDate.now().getYear() + 1;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR
    public Vehiculo(String Placa, short Modelo, String Color, String Marca) throws Exception {
        if (Placa == null || "".equals(Placa.trim())) {
            throw new Exception("la Placa no puede ser vacio o null");
        }
        if (Placa.length() != 6) {
            throw new Exception("La placa debe tener 6 dijitos");

        }
        if (Color == null || "".equals(Color.trim())) {
            throw new Exception("El Color no puede ser vacio o null");
        }
        if (Marca == null || "".equals(Marca.trim())) {
            throw new Exception("El nombre no puede ser vacio o null");
        }
        if (Modelo <= 1900 || Modelo >= Añovalido) {
            throw new Exception("El modelo del Vehiculo debe ser mayor a 1900 y menor o igual" + Añovalido);
        }
        this.Placa = Placa;
        this.Modelo = Modelo;
        this.Color = Color;
        this.Marca = Marca;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // METODOS GET
    public String getPlaca() {
        return Placa;
    }

    public short getModelo() {
        return Modelo;
    }

    public String getColor() {
        return Color;
    }

    public String getMarca() {
        return Marca;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    // METODOS SET
    public void setPlaca(String Placa) throws Exception {
        if (Placa == null || "".equals(Placa.trim())) {
            throw new Exception("la Placa no puede ser vacio o null");
        }
        if (Placa.length() != 6) {
            throw new Exception("La placa debe tener 6 dijitos");

        }
        this.Placa = Placa;
    }

    public void setModelo(short Modelo) throws Exception {
        if (Modelo <= 1900 || Modelo >= Añovalido) {
            throw new Exception("El modelo del Vehiculo debe ser mayor a 1900 y menor o igual" + Añovalido);
        }
        this.Modelo = Modelo;
    }

    public void setColor(String Color) throws Exception {
        if (Color == null || "".equals(Color.trim())) {
            throw new Exception("El Color no puede ser vacio o null");
        }
        this.Color = Color;
    }

    public void setMarca(String Marca) throws Exception {
        if (Marca == null || "".equals(Marca.trim())) {
            throw new Exception("El nombre no puede ser vacio o null");
        }
        this.Marca = Marca;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // EQUALS
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;

        if (!Objects.equals(this.Placa, other.Placa)) {
            return false;
        }

        return true;
    }

}
