package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan Camilo Castillo 1763929-2711
 */
public class Agente extends Persona {

    private short numeroPlaca;
    //////////////////////////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR

    public Agente(short numeroPlaca, long identificacion, String Nombre, String Apellido) throws Exception {
        super(identificacion, Nombre, Apellido);
        if (numeroPlaca <= 999 || numeroPlaca > 9999) {
            throw new Exception("el numero de la placa tiene que ser de cuatro digitos");
        }
        this.numeroPlaca = numeroPlaca;

    }

    /////////////////////////////////////////////////////////////////////////////////////////
    //METODOS GET
    public short getNumeroPlaca() {
        return numeroPlaca;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    //METODOS SET
    public void setNumeroPlaca(short numeroPlaca) throws Exception {
        if (numeroPlaca <= 999 || numeroPlaca > 9999) {
            throw new Exception("el numero de la placa tiene que ser de cuatro digitos");
        }

        this.numeroPlaca = numeroPlaca;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    //EQUALS
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agente other = (Agente) obj;
        if (this.numeroPlaca != other.numeroPlaca) {
            return false;
        }
        return true;
    }

}
