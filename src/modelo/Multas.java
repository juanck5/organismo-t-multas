package modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author juan Camilo Castillo 1763929-2711
 */
import java.util.LinkedList;
import java.util.Iterator;
import java.util.List;
import java.util.Date;
import java.util.Objects;

public class Multas {

    private int Valor;
    private Vehiculo vehiculo; //1 a 1 con veiculo
    private Agente agente;  //1 a 1 con agente
    private Persona conductor;  //1 a 1 con persona
    private Date Date;
    // 1..* con motivo multa
    private List<MotivoMulta> MMulta = new LinkedList<>();
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //constructor

    public Multas(int Valor, Vehiculo veiculo, Agente agente, Persona conductor, Date Date) throws Exception {
        if (Valor <= 0) {
            throw new Exception("el valor no puede ser 0");
        }
        this.Valor = Valor;
        this.vehiculo = vehiculo;
        this.agente = agente;
        this.conductor = conductor;
        this.Date = Date;

    }
    ///////////////////////////////////////////////////////////////////////////
    //metodo agregar un motivo de multa 

    public void add(MotivoMulta MM) throws Exception {
        for (MotivoMulta Mm : MMulta) {
            if (MM.equals(Mm)) {
                throw new Exception("EL Motivo multa ya esta agregado");
            }
        }
        this.MMulta.add(MM);
        System.out.println("El Motivo multa fue agregado");
    }

    ///////////////////////////////////////////////////////////////////////////
    //merodo Buscar un objeto de la lista Motivo multa
    public MotivoMulta BuscarMM(MotivoMulta Mm) throws Exception {
        if (MMulta.contains(Mm)) {
            return MMulta.get(MMulta.indexOf(Mm));
        } else {
            throw new Exception("el motivo multa ya esta agregado");
        }

    }

    //Metodo eliminar un motivo de multa
    public void remove(MotivoMulta MM) {
        this.MMulta.remove(MM);
    }

    //metodos get
    public int getValor() {
        return this.Valor;
    }

    public Vehiculo getVehiculo() {
        return this.vehiculo;
    }

    public Agente getAgente() {
        return this.agente;
    }

    public Persona getConductor() {
        return this.conductor;
    }

    //metodos Set;
    public void setValor(int Valor) throws Exception {
        if (Valor <= 0) {
            throw new Exception("el valor no puede ser 0");
        }
        this.Valor = Valor;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }
    //equals

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Multas other = (Multas) obj;
        if (this.Valor != other.Valor) {
            return false;
        }
        if (!Objects.equals(this.vehiculo, other.vehiculo)) {
            return false;
        }
        if (!Objects.equals(this.agente, other.agente)) {
            return false;
        }
        if (!Objects.equals(this.conductor, other.conductor)) {
            return false;
        }
        if (!Objects.equals(this.Date, other.Date)) {
            return false;
        }
        if (!Objects.equals(this.MMulta, other.MMulta)) {
            return false;
        }
        return true;

    }

}
