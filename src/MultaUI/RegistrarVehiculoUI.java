/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MultaUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import modelo.OrgTransito;
import modelo.Vehiculo;

/**
 *
 * @author juan
 */
public class RegistrarVehiculoUI extends javax.swing.JInternalFrame {

    private OrgTransito orgT;
    boolean activas = false;

    /**
     * Creates new form RegistrarVehiculoUI
     *
     * @param
     */
    public RegistrarVehiculoUI(OrgTransito orgt2) {
        this.orgT = orgt2;
        initComponents();
        ////////////////////////////////////////////////////////////////////////
        //AGREGAR CONTROLADOR A LOS BOTONES ////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        //BOTON REGISTRAR///////////////////////////////////////////////////////
        BregistrarVehiculos brv = new BregistrarVehiculos();
        btnregistara.addActionListener(brv);
        //BOTON cANCELAR////////////////////////////////////////////////////////
        ManejadorLimpiar manlimp = new ManejadorLimpiar();
        btncancelar.addActionListener(manlimp);
        //BOTON BUSCAR//////////////////////////////////////////////////////////
        ManjadorBuscar mbuscar = new ManjadorBuscar();
        btnbuscar.addActionListener(mbuscar);
        texplaca.addActionListener(mbuscar);
    }

    private RegistrarVehiculoUI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        texplaca = new javax.swing.JTextField();
        texcolor = new javax.swing.JTextField();
        texmodelo = new javax.swing.JTextField();
        boxmarca = new javax.swing.JComboBox<>();
        btnregistara = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        btnbuscar = new javax.swing.JButton();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel1.setFont(new java.awt.Font("Noto Sans", 1, 12)); // NOI18N
        jLabel1.setText("Registro  de Vehiculos");

        jLabel2.setText("Placa :");

        jLabel3.setText("Marca :");

        jLabel4.setText("Modelo :");

        jLabel5.setText("Color :");

        texplaca.setBackground(java.awt.Color.white);
        texplaca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                texplacaActionPerformed(evt);
            }
        });

        texcolor.setBackground(java.awt.Color.white);
        texcolor.setEnabled(false);

        texmodelo.setBackground(java.awt.Color.white);
        texmodelo.setEnabled(false);

        boxmarca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "audi", "skoda", "mazda", "renault" }));

        btnregistara.setText("Registrar");

        btncancelar.setText("Cancelar");

        btnbuscar.setText("Busca");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(9, 9, 9)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(texcolor)
                            .addComponent(texmodelo)
                            .addComponent(boxmarca, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnregistara, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(texplaca, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnbuscar))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(texplaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnbuscar))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(boxmarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(texmodelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(texcolor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnregistara, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void texplacaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_texplacaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_texplacaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarVehiculoUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarVehiculoUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarVehiculoUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarVehiculoUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistrarVehiculoUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> boxmarca;
    private javax.swing.JButton btnbuscar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnregistara;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField texcolor;
    private javax.swing.JTextField texmodelo;
    private javax.swing.JTextField texplaca;
    // End of variables declaration//GEN-END:variables
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//MANEJADOR DE EVENTOS REGISTRAR VEHICULOS////////////////////////////////////////////////////////////////

    public class BregistrarVehiculos implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                ////////////////////////////////////////////////////////////////////////////
                // 1 OBTENER LA INFORMACION DE LA VENTANA////////////////////////////////////

                String placa = texplaca.getText();
                short modelo = Short.parseShort(texmodelo.getText());
                String color = texcolor.getText();
                String marca = (String) boxmarca.getSelectedItem();
                /////////////////////////////////////////////////////////////////////////////
                //2 CREAR EL OBJETO//////////////////////////////////////////////////////////

                Vehiculo vehiculo1 = new Vehiculo(placa, modelo, color, marca);
                ///////////////////////////////////////////////////////////////////////////
                // 3 AGEGAR EL OBJETO///////////////////////////////////////////////////////
                orgT.addVehiculo(vehiculo1);
                JOptionPane.showMessageDialog(rootPane, "se registro con Exito");
                texplaca.setText("");
                texmodelo.setText("");
                texcolor.setText("");
                boxmarca.setSelectedItem(null);
                texcolor.setEnabled(false);
                boxmarca.setEnabled(false);
                texmodelo.setEnabled(false);
            } catch (NumberFormatException exc) {
                Logger.getAnonymousLogger().log(Level.SEVERE, null, exc);
                JOptionPane.showMessageDialog(rootPane, exc.getMessage());

            } catch (Exception exc) {
                JOptionPane.showMessageDialog(rootPane, exc.getMessage());
                //guardar los errores en un documento
                Logger.getAnonymousLogger().log(Level.SEVERE, exc.getMessage(), exc);
            }

        }

    }
    ////////////////////////////////////////////////////////////////////////////////////////////////   
//MANEJADOR DE EVENTOS PARA EN BOTON LIMPIAR/////////////////////////////////////////////////////

    public class ManejadorLimpiar implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            texcolor.setText("");
            texmodelo.setText("");
            texplaca.setText("");
            boxmarca.setSelectedItem(null);
            texmodelo.setEnabled(false);
            texcolor.setEnabled(false);
            boxmarca.setEnabled(false);
            btnregistara.setEnabled(false);
        }

    }
    ////////////////////////////////////////////////////////////////////////////////
    //MANEJADOR EVENTOS BUSCAR/////////////////////////////////////////////////////

    public class ManjadorBuscar implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                //obtener placa
                String placa = texplaca.getText();
                //buscar el vehiculo
                Vehiculo veh = orgT.BuscarV(placa);
                //mostrar la informacion
                texplaca.setText(veh.getPlaca());
                texmodelo.setText(Short.toString(veh.getModelo()));
                texcolor.setText(veh.getColor());
                boxmarca.setSelectedItem(veh.getMarca());
                texmodelo.setEnabled(true);
                boxmarca.setEnabled(true);
                texcolor.setEnabled(true);
                btnregistara.setEnabled(true);

            } catch (Exception exc) {
                String msg = "No se encontro el vehiculo. Desea crearlo";
                int option = JOptionPane.showConfirmDialog(rootPane, msg);
                if (option == JOptionPane.YES_OPTION) {
                    texmodelo.setText("");
                    texmodelo.setText("");
                    boxmarca.setSelectedItem(null);

                    texmodelo.setEnabled(true);
                    boxmarca.setEnabled(true);
                    texcolor.setEnabled(true);
                    btnregistara.setEnabled(true);

                }
            }
        }

    }

}
